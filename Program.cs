using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
//using OpenQA.Selenium.WebDriver;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;


namespace ConsoleApp1
{
    class Program
    {

        static void Main(string[] args)
        {
            IWebDriver driver;
            // Caso 1 - Validar campo de texto
            driver = new ChromeDriver();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

            //Caso 1
            Console.WriteLine("Incia caso 1");
            driver.Navigate().GoToUrl("https://www.lineru.com/");
            driver.Manage().Window.Maximize();
            driver.FindElement(By.Id("mat-input-0")).Clear();
            driver.FindElement(By.Id("mat-input-0")).SendKeys("150000");
            //Screenshot s150000 = ((ITakesScreenshot)driver).GetScreenshot();
            //s150000.SaveAsFile(@"D:\Mis Archivos\docs\lineru\screenshot");
            driver.FindElement(By.Id("mat-input-0")).Clear();
            driver.FindElement(By.Id("mat-input-0")).SendKeys("599999");
            // Screenshot s59999 = ((ITakesScreenshot)driver).GetScreenshot();
            //s150000.SaveAsFile(@"D:\Mis Archivos\docs\lineru\screenshot");
            driver.FindElement(By.Id("mat-input-0")).Clear();
            //Solo esta controlando cuando es mayor a 609000
            driver.FindElement(By.Id("mat-input-0")).SendKeys("600001");
            //Screenshot s600001 = ((ITakesScreenshot)driver).GetScreenshot();
            //s150000.SaveAsFile(@"D:\Mis Archivos\docs\lineru\screenshot");
            driver.FindElement(By.Id("mat-input-0")).Clear();
            driver.FindElement(By.Id("mat-input-0")).SendKeys("610000");
            //Screenshot s610000 = ((ITakesScreenshot)driver).GetScreenshot();
            //s150000.SaveAsFile(@"D:\Mis Archivos\docs\lineru\screenshot");
            Console.WriteLine("Finalizó caso 1");
            
            ////Caso 2----------------NO EJECUTAR--------------
            //Console.WriteLine("Incia caso 2");
            //driver.FindElement(By.Id("mat-input-0")).Clear();
            //driver.FindElement(By.Id("mat-input-0")).SendKeys("600000");
            ////var valorsolicitado = driver.FindElement(By.XPath("/html/body/app-root/app-init/div[1]/div[2]/div[3]/shared-calculator/div/div[2]/div/div[1]/div/ul/li[1]/div[2]/strong")).GetAttribute("h6 text - dark");
            //var valorsolicitado = driver.FindElement(By.XPath("/html/body/app-root/app-init/div[1]/div[2]/div[3]/shared-calculator/div/div[2]/div/div[1]/div/ul/li[1]/div[2]/strong"));
            //var ece =valorsolicitado.GetAttribute("_ngcontent-serverapp-c79");
            //Console.WriteLine(ece);
            //string valor = "$600,000";

            //if (valorsolicitado.Equals(valor))
            //{
            //    Console.WriteLine("PASSED");

            //}
            //else
            //{

            //    Console.WriteLine("FAILED");
            //}
            ////driver.FindElement(By.XPath("/html/body/app-root/app-init/div[1]/div[2]/div[3]/shared-calculator/div/div[2]/div/div[1]/div/ul/li[2]/div[2]/span")).Click();
            ////driver.FindElement(By.XPath("/html/body/app-root/app-init/div[1]/div[2]/div[3]/shared-calculator/div/div[2]/div/div[1]/div/ul/li[4]/div[2]/span")).Click();
            ////driver.FindElement(By.XPath("/html/body/app-root/app-init/div[1]/div[2]/div[3]/shared-calculator/div/div[2]/div/div[2]/div/ul/li[1]/div[2]/strong")).Click();
            ////driver.FindElement(By.XPath("/html/body/app-root/app-init/div[1]/div[2]/div[3]/shared-calculator/div/div[2]/div/div[2]/div/ul/li[3]/div[2]/span")).Click();
            ////driver.FindElement(By.XPath("/html/body/app-root/app-init/div[1]/div[2]/div[3]/shared-calculator/div/div[2]/div/div[2]/div/ul/li[4]/div[2]/span")).Click();
            ////driver.FindElement(By.XPath("/html/body/app-root/app-init/div[1]/div[2]/div[3]/shared-calculator/div/div[2]/div/div[3]/div/ul/li/div[2]/span")).Click();
            ////Console.WriteLine("Finalizó caso 2");
            //-----------------------------------------------------------------------------------------------

            // CASO 3 -- Ingresar datos en el formulario
            driver.FindElement(By.Id("mat-input-0")).Clear();
            driver.FindElement(By.Id("mat-input-0")).SendKeys("150000");
            driver.FindElement(By.Id("mat-input-0")).SendKeys(Keys.Enter);

            //Click en Solcita tu crédito

            //var button = driver.FindElement(By.XPath("/html/body/app-root/app-init/div[1]/div[2]/div[3]/shared-calculator/div/div[1]/div/form/button[1]"));
            //Actions actions = new Actions(driver);
            //actions.MoveToElement(button);
            //actions.Perform();
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            //driver.FindElement(By.TagName("SOLICITA TU CRÉDITO")).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.FindElement(By.Id("mat-input-2")).SendKeys("Andres Alzate");
            driver.FindElement(By.Id("mat-select-0")).SendKeys(Keys.ArrowDown);
            driver.FindElement(By.Id("mat-input-3")).SendKeys("1010101000");
            driver.FindElement(By.Id("mat-input-4")).SendKeys("3127152385");
            driver.FindElement(By.Id("mat-input-5")).SendKeys("pipe0607@hotmail.com");
            driver.FindElement(By.Id("mat-input-6")).SendKeys("test1");
            driver.FindElement(By.Id("mat-input-6")).SendKeys(Keys.Tab);
            //Chequeo los terminos y condiciones y tratamiento de datos
            IWebElement check = driver.FindElement(By.XPath("/html/body/app-root/app-auth-register/div/div[2]/div/div/div[1]/div/form/div[2]/div/app-checkbox[1]/div/div[1]/div"));
            Actions actions = new Actions(driver);
            actions.MoveToElement(check);
            actions.Perform();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            //check.Click();
            //Chequeo tratamiento de datos
            IWebElement check2 = driver.FindElement(By.XPath("/html/body/app-root/app-auth-register/div/div[2]/div/div/div[1]/div/form/div[2]/div/app-checkbox[2]/div/div/div"));
            Actions actions2 = new Actions(driver);
            actions.MoveToElement(check);
            actions.Perform();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            //check.Click();
            //check2.Click();
            //Screenshot s150000 = ((ITakesScreenshot)driver).GetScreenshot();
            //s150000.SaveAsFile(@"D:\Mis Archivos\docs\lineru\screenshot");
            Console.WriteLine("Finalizó Caso 3");
            Console.WriteLine("Finalizó el set de pruebas");
            driver.Close();

        }
    }
    }

